module ImageTest where

import Image

ok = and [
       shouldPutCell,
       shouldNotPutCell
     ]

shouldPutCell = putCell image1 (Pos 1 1) 1 == Just image2
shouldNotPutCell = putCell image2 (Pos 0 1) 1 == Nothing

image1 :: Image Int
image1 = Image (Dims 3 2)
               (map (\x -> if x /= 4 then Just 1 else Nothing) [0..5])

image2 :: Image Int
image2 = Image (Dims 3 2) (replicate 6 (Just 1))

