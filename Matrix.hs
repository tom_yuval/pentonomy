module Matrix (
         module Matrix,
         module Data.Array
       ) where

import Data.Array
import Data.List (transpose, intercalate)
import Data.Set (Set, singleton)

import Closure

data Pos = Pos { row :: Int, col :: Int } deriving (Eq, Ord, Ix, Show)
origin = Pos 0 0
(Pos r1 c1) |+| (Pos r2 c2) = Pos (r1+r2) (c1+c2)
(Pos r1 c1) |-| (Pos r2 c2) = Pos (r1-r2) (c1-c2)

data Dims = Dims { rows :: Int, cols :: Int } deriving (Eq, Ord, Show)
size (Dims r c) = r * c
maxPos (Dims r c) = Pos (r-1) (c-1)
fromMaxPos (Pos r c) = Dims (r+1) (c+1)
posRange dims = (origin, maxPos dims)

type Matrix = Array Pos
fromList dims = listArray $ posRange dims
makeMatrix dims value = fromList dims (replicate (size dims) value)
fromRows rs = fromList (Dims (length rs) $ length $ rs!!0) $ concat rs
dims = fromMaxPos.snd.bounds
maxRow = row.maxPos.dims
maxCol = col.maxPos.dims

flipRows = fromRows.reverse.getRows
rotate = fromRows.reverse.transpose.getRows

allSymmetries :: Ord a => Matrix a -> Set (Matrix a)
allSymmetries = (close [flipRows, rotate]).singleton

serialize elemSerializer rowSep colSep =
  intercalate rowSep . serializeRows elemSerializer colSep
serializeRows elemSerializer colSep =
  map (serializeRow elemSerializer colSep) . getRows
serializeRow elemSerializer colSep =
  intercalate colSep . map elemSerializer
getRows matrix = [getRow matrix row | row <- [0..maxRow matrix]]
getRow matrix row = [matrix!(Pos row col) | col <- [0..maxCol matrix]]

