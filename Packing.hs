module Packing where

import Data.Set (Set, empty, elems, delete)
import Data.List (find)

import Matrix hiding (elems)
import Piece
import Board

pack :: (Eq a, Ord a) => Board a -> Set (Set (Piece a)) -> [Board a]
pack board pieceForms
  | pieceForms == empty = [board]
  | otherwise = packGivenFreeCell board (findFreeCell board) pieceForms

findFreeCell :: Eq a => Board a -> Maybe Pos
findFreeCell board = find (cellIsFree board) . range . posRange $ dims board

packGivenFreeCell :: Ord a => Board a ->
                              Maybe Pos ->
                              Set (Set (Piece a)) ->
                              [Board a]
packGivenFreeCell board Nothing _ = [board]
packGivenFreeCell board (Just freeCell) pieceForms =
  concat [packGivenFreeCellAndPiece board freeCell piece remainingPieces |
          (piece, remainingPieces) <- allOptionsFromSet pieceForms]

packGivenFreeCellAndPiece :: Ord a => Board a ->
                                      Pos ->
                                      Set (Piece a) ->
                                      Set (Set (Piece a)) ->
                                      [Board a]
packGivenFreeCellAndPiece board cell piece remainingPieces =
  concat [packGivenFreeCellAndPieceForm board cell pieceForm remainingPieces |
          pieceForm <- elems piece]

packGivenFreeCellAndPieceForm :: Ord a => Board a ->
                                          Pos -> 
                                          Piece a -> 
                                          Set (Set (Piece a)) -> 
                                          [Board a]
packGivenFreeCellAndPieceForm board cell piece@(Piece form _) remainingPieces =
  concat [pack b remainingPieces |
          b <- removeNothings $ map (put board piece) $ map (cell |-|) $ filter (form!) . range . posRange $ dims form]

removeNothings :: [Maybe a] -> [a]
removeNothings [] = []
removeNothings (Nothing:xs) = removeNothings xs
removeNothings (Just x:xs) = x:removeNothings xs

allOptionsFromSet :: Ord a => Set a -> [(a, Set a)]
allOptionsFromSet set = [(elem, delete elem set) | elem <- elems set]

