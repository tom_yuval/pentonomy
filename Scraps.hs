module Scraps where

import Matrix

--q = Piece ((makeMatrix (Dims 2 4) False)//[(p, True) | p <- [Pos 0 0, Pos 0 1, Pos 0 2, Pos 0 3, Pos 1 0]]) "Q"
p = putStrLn $ serialize strip "\n" ""

strip :: Maybe String -> String
strip Nothing = " "
strip (Just s) = s

fromChar c = if c == ' ' then Nothing else (Just [c])

