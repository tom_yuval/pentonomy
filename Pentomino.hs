module Pentomino where

import Data.Set (fromList)

import Matrix
import Piece
import Board

data PentominoType = O | P | Q | R | S | T | U | V | W | X | Y | Z
                     deriving (Eq, Ord, Show)

toString :: Maybe PentominoType -> String
toString Nothing = " "
toString (Just t) = show t

type PentominoPiece = Piece PentominoType

makePentomino :: [String] -> PentominoType -> PentominoPiece
makePentomino rows = Piece $ fromRows (map (map (/= ' ')) rows)

baseForms = [makePentomino ["*****"] O,
             makePentomino ["***",
                            "** "] P,
             makePentomino ["****",
                            "*   "] Q,
             makePentomino ["** ",
                            " **",
                            " * "] R,
             makePentomino ["*** ",
                            "  **"] S,
             makePentomino ["***",
                            " * ",
                            " * "] T,
             makePentomino ["***",
                            "* *"] U,
             makePentomino ["***",
                            "*  ",
                            "*  "] V,
             makePentomino ["** ",
                            " **",
                            "  *"] W,
             makePentomino [" * ",
                            "***",
                            " * "] X,
             makePentomino ["****",
                            " *  "] Y,
             makePentomino ["** ",
                            " * ",
                            " **"] Z]

pentominoSet = Data.Set.fromList $ map allForms baseForms

type PentominoBoard = Board PentominoType

serializePentominoBoard :: PentominoBoard -> String
serializePentominoBoard board = serialize toString "\n" "" board ++ "\n"

