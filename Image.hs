{-# LANGUAGE DatatypeContexts #-}

module Image (
         Dims(..),
         Pos(..),
         Image(..),
         emptyImage,
         getCell,
         putCell
       ) where

replace :: [a] -> Int -> a -> [a]
replace [] _ _ = undefined
replace (x:xs) 0 y = y:xs
replace (x:xs) n y = x:(replace xs (n-1) y)

data Dims = Dims { width :: Int, height :: Int } deriving (Eq, Ord, Show)
size (Dims w h) = w * h

data Pos = Pos { x :: Int, y :: Int }

index dims pos = (y pos) * (width dims) + (x pos)

data (Eq a) => Image a = Image { dims :: Dims, values :: [Maybe a] }
               deriving (Eq, Ord, Show)

--data Image a where
--  Image :: Eq a => Dims -> [Maybe a] -> Image a
--dims (Image d _) = d
--values (Image _ v) = v
--
--instance Eq a => Eq (Image a) where
--  Image dims1 values1 == Image dims2 values2 = dims1 == dims2 &&
--                                               values1 == values2
--
--instance Show a => Show (Image a) where
--  show (Image d v) = (show d) ++ " " ++ (show v)

emptyImage dims = Image dims (replicate (size dims) Nothing)

getCell image pos = (values image) !! (index (dims image) pos)

putCell :: (Eq a) => Image a -> Pos -> a -> Maybe (Image a)
putCell image@(Image d v) pos value
  | getCell image pos /= Nothing = Nothing
  | otherwise = Just (image { values = replace v (index d pos) (Just value) })

--put :: Image a -> Image a -> Pos -> Image a
--put board piece pos

