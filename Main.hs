module Main where

import Matrix
import Board
import Packing
import Pentomino

main = mapM_ (putStrLn.serializePentominoBoard) $ pack (emptyBoard $ Dims 10 6) pentominoSet

