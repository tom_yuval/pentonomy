module Closure(
         close
       ) where

import Data.Set

close functions set
  | added `isSubsetOf` set = set
  | otherwise = close functions (set `union` added)
  where added = unions [Data.Set.map function set | function <- functions]


