module Board where

import Matrix
import Piece

type Board a = Matrix (Maybe a)
emptyBoard dims = makeMatrix dims Nothing

put :: Eq a => Board a -> Piece a -> Pos -> Maybe (Board a)
put board (Piece s v) pos
  | canPut board s pos = Just $ setCells board (targetCells s pos) v
  | otherwise = Nothing

canPut :: Eq a => Board a -> Shape -> Pos -> Bool
canPut b s p = fits (dims b) (dims s) p && cellsAreFree b s p

fits :: Dims -> Dims -> Pos -> Bool
fits boardDims pieceDims pos =
  let onBoard = inRange (posRange boardDims) in
  onBoard pos && onBoard (pos |+| maxPos pieceDims)

cellsAreFree :: Eq a => Board a -> Shape -> Pos -> Bool
cellsAreFree board s pos = all (cellIsFree board) $ targetCells s pos

cellIsFree :: Eq a => Board a -> Pos -> Bool
cellIsFree board pos = board!pos == Nothing

targetCells s pos = map (pos|+|) (occupiedCells s)

setCells :: Board a -> [Pos] -> a -> Board a
setCells board ps v = board//[(p, Just v) | p <- ps]

