module Piece where

import Data.Set (map)

import Matrix

type Shape = Matrix Bool
occupiedCells s = filter (s!) (range $ posRange $ dims s)

data Piece a = Piece { shape :: Shape, value :: a } deriving (Eq, Ord, Show)

allForms (Piece s v) = Data.Set.map (\m -> Piece m v) $ allSymmetries s

